#!/bin/bash

active_connection=`awk '/\*/ {print $2}' <(netctl list)`
red='\e[0;31m'
noc='\e[0m' 
_get_connection() {
    echo -e "1)$red devnull$noc\n2)$red eduroam$noc\nSelect a connection [o=1, t=2, q=quit]: " 
    read otq

    case $otq in 
        o)
            sudo ip link set wlp7s0 down; sudo netctl start devnull
            ;;
        t)
            sudo ip link set wlp7s0 down; sudo netctl start eduroam
            ;;
        q)
            exit
            ;;
        *)
            printf "%s\n%s" "Select a connection" "o->one, t->two"
            ;;
    esac
}

if [[ $active_connection ]]; then
    echo -e "\nAlready connected to $red$active_connection$noc\n"
    sleep 5
else
    _get_connection
fi
