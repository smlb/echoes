#!/bin/bash

BACKUP_DIR=/home/smlb/docs/backup/
DATE=`echo -e "$(date '+%d-%m_%I-%M%p')"`
BACKUP=`tar czvf backup_i3_$DATE.tar.gz /home/smlb/.config/{i3,i3status}`
#CLEAN=`find /home/smlb/docs/backup/test1/*.tar.gz -type f -mmin +2 -exec rm {} \;`

if [[ -d /home/smlb/docs/backup/ ]]; then
  cd $BACKUP_DIR
  echo -e "$BACKUP" > /dev/null
fi

