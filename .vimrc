"
"
" Smlb's ~/.vimrc
" Last Update: 13/12/14 12:11AM
"
" 

filetype plugin on
filetype indent on
    
" Some options
set nocompatible
set showmatch
set wildmenu
set number
set ttyfast
set cmdheight=2
set backspace=eol,start,indent
set ignorecase
set incsearch
set magic
set background=dark
set t_Co=256
set shell=/bin/zsh
set mouse=a
set autoread
set smartcase
set visualbell
set linebreak
set textwidth=0
set display=lastline

" Don't save nothing
set noswapfile
set nobackup
set nowb

" Encoding
set termencoding=utf-8
set encoding=utf-8
scriptencoding utf-8
set ffs=unix

set noerrorbells
map <C-n> :NERDTreeToggle<CR>

" Vundle Shit here
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Bundle 'chrisbra/distractfree.vim'
Bundle 'itchyny/lightline.vim'
Bundle 'lervag/vim-latex'
call vundle#end()

" Airline options
let g:lightline = {
      \ 'colorscheme': 'Tomorrow_Night',
      \ 'component': {
      \   'readonly': '%{&readonly?"x":""}',
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '|', 'right': '|' }
      \ }
  
syntax enable
colorscheme sobrio

" Some indent commands
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set lbr
set tw=500
set ai 
set si 
set wrap 
set laststatus=2
     
set pastetoggle=<F2>

" Modeline after last buffer
function! AppendModeline()
    let l:modeline = printf("vim: set ft=%s ts=%d sw=%d tw=%d %set :",
                \ &filetype, &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
    let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
    call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

"vim: set ft=vim ts=2 sw=2 tw=500 et :
